package org.clevertec.project.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.clevertec.project.dto.History;
import org.clevertec.project.dto.FilterForHistory;
import org.clevertec.project.service.HistoryService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/history")
@Slf4j
@RequiredArgsConstructor
public class HistoryController {

    private final HistoryService service;

    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> save(@RequestBody History history) {
        try {
            return ResponseEntity.ok(service.save(history));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/get/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findById(@PathVariable String id) {
        Optional<History> history = service.findById(id);
        if (history.isPresent()) {
            return ResponseEntity.ok(history);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/get/all", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findAll(
            @RequestParam(name = "pagesize", defaultValue = "15", required = false)
                    Integer pageSize,
            @RequestParam(name = "pagenumber", defaultValue = "0", required = false)
                    Integer pageNumber) {
        try {
            return ResponseEntity.ok(service.findAll(pageSize, pageNumber));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/get/some", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findSome(@RequestParam(name = "pagesize", defaultValue = "15", required = false)
                                              Integer pageSize,
                                      @RequestParam(name = "pagenumber", defaultValue = "0", required = false)
                                              Integer pageNumber,
                                      @RequestBody FilterForHistory filter) {
        try {
            return ResponseEntity.ok(service.findSome(pageSize, pageNumber, filter));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> update(@RequestBody History history) {
        Optional<History> historyFind = service.findById(history.getId());
        if (historyFind.isPresent()) {
            return ResponseEntity.ok(service.update(history));
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(value = "/delete/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> delete(@PathVariable String id) {
        Optional<History> historyFind = service.findById(id);
        if (historyFind.isPresent()) {
            return ResponseEntity.ok(service.delete(id));
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(value = "/delete/all", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> deleteAll() {
        try {
            return ResponseEntity.ok(service.deleteAll());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
