package org.clevertec.project.repository;

import org.clevertec.project.dto.History;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface HistoryRepository extends MongoRepository<History, String> {
}
