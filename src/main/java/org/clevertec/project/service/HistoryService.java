package org.clevertec.project.service;

import org.clevertec.project.dto.History;
import org.clevertec.project.dto.FilterForHistory;
import org.springframework.data.domain.Page;

import java.util.Optional;

public interface HistoryService {
    String save(History history);

    Optional<History> findById(String id);

    Page<History> findAll(Integer pageSize, Integer pageNumber);

    Page<History> findSome(Integer pageSize, Integer pageNumber, FilterForHistory filter) throws NoSuchFieldException, IllegalAccessException;

    String update(History history);

    String delete(String id);

    int deleteAll();
}
