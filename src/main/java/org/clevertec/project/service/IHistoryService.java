package org.clevertec.project.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.clevertec.project.dto.History;
import org.clevertec.project.dto.FilterForHistory;
import org.clevertec.project.repository.HistoryRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.*;

@Service
@Slf4j
@RequiredArgsConstructor
public class IHistoryService implements HistoryService {

    private final HistoryRepository repository;
    private final ObjectMapper objectMapper;
    private final MongoTemplate mongoTemplate;

    private Date date;

    @Override
    public String save(History history) {
        repository.save(history);
        log.info("History {} added successful", toJson(history));
        return history.getId();
    }

    @Override
    public Optional<History> findById(String id) {
        Optional<History> history = repository.findById(id);
        log.info("History with id = {} received successful", id);
        return history;
    }

    @Override
    public Page<History> findAll(Integer pageSize, Integer pageNumber) {
        Page<History> histories = repository.findAll(PageRequest.of(pageNumber, pageSize));
        log.info("History pageable received successful : {}", toJson(histories));
        return histories;
    }

    @Override
    public Page<History> findSome(Integer pageSize, Integer pageNumber, FilterForHistory filter) throws NoSuchFieldException, IllegalAccessException {
        final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
        Query query = new Query();
        query = createQuery(query, filter);
        query.with(pageableRequest);
        List<History> list = mongoTemplate.find(query, History.class);
        Page<History> page = new PageImpl<>(list, pageableRequest, list.size());
        log.info("History pageable received successful : {}", toJson(page));
        return page;
    }

    @Override
    public String update(History history) {
        repository.save(history);
        log.info("History {} update successful", toJson(history));
        return history.getId();
    }

    @Override
    public String delete(String id) {
        repository.deleteById(id);
        log.info("History with id = {} deleted successful", id);
        return id;
    }

    @Override
    public int deleteAll() {
        List<History> list = repository.findAll();
        repository.deleteAll();
        log.info("All histories deleted successful");
        return list.size();
    }

    private synchronized String toJson(Object o) {
        String json = null;
        try {
            json = objectMapper.writeValueAsString(o);
        } catch (JsonProcessingException e) {
            log.warn("can't represent object of class {} in json form for logging: {}",
                    o.getClass().getSimpleName(), e.toString());
        }
        return json;
    }

    private Query createQuery(Query query, FilterForHistory filter) throws NoSuchFieldException, IllegalAccessException {
        Class filterClass = FilterForHistory.class;
        Field[] fields = filterClass.getDeclaredFields();
        query = createCriteriaWithDifferentValues(fields, query, filter);
        return query;
    }

    private Query createCriteriaWithDifferentValues(Field[] fields, Query query, FilterForHistory filter) throws NoSuchFieldException, IllegalAccessException {
        Class serviceClass = IHistoryService.class;
        Field dateField = serviceClass.getDeclaredField("date");
        Object dateType = dateField.getType();
        LinkedHashMap<String, String> historyFields = new LinkedHashMap<>();
        LinkedHashMap<String, Object> filterValues = new LinkedHashMap<>();
        for(Field field: fields){
            Object filterFieldType = field.getType();
            String filterFieldName = field.getName();
            field.setAccessible(true);
            String historyFieldName = getHistoryFieldName(filterFieldName);
            if (filterFieldType.equals(dateType)){
                Object filterFieldValue = field.get(filter);
                historyFields.put(filterFieldName, historyFieldName);
                filterValues.put(filterFieldName, filterFieldValue);
            } else {
                Object[] filterFieldValue = (Object[]) field.get(filter);
                query = createCriterionWithOneValue(query, filterFieldValue, historyFieldName);
            }
        }
        query =  createCriteriaWithSomeValues(query, historyFields, filterValues);
        return query;
    }

    private String getHistoryFieldName(String filterFieldName){
        Class historyClass = History.class;
        Field[] fields = historyClass.getDeclaredFields();
        String historyFieldName = "";
        for(Field field: fields){
            historyFieldName = field.getName();
            boolean check = filterFieldName.contains(historyFieldName);
            if (check){
                break;
            }
        }
        return historyFieldName;
    }

    private Query createCriteriaWithSomeValues(Query query, LinkedHashMap<String, String> fieldsNames,
                                               LinkedHashMap<String, Object> fieldsValues){
        if(fieldsNames.isEmpty() && fieldsValues.isEmpty()){
            return query;
        } else {
            Object[] filterFieldValues = new Object[2];
            String historyFieldName = "";
            for(Map.Entry<String, String> entry : fieldsNames.entrySet()){
                if(!historyFieldName.equals(entry.getValue())){
                    filterFieldValues[0] = fieldsValues.get(entry.getKey());
                }
                if (historyFieldName.equals(entry.getValue())){
                    filterFieldValues[1] = fieldsValues.get(entry.getKey());
                    query = createCriterionWithTwoValues(query, filterFieldValues, historyFieldName);
                }
                historyFieldName = entry.getValue();
            }
            return query;
        }
    }

    private Query createCriterionWithTwoValues(Query query, Object[] values, String name){
        if (values[0] != null && values[1] != null) {
            query.addCriteria(Criteria.where(name).gt(values[0]).lt(values[1]));
        } else if (values[0] != null) {
            query.addCriteria(Criteria.where(name).gt(values[0]));
        } else if (values[1] != null) {
            query.addCriteria(Criteria.where(name).lt(values[1]));
        }
        return query;
    }

    private Query createCriterionWithOneValue(Query query, Object[] value, String historyFieldName){
        if(value.length != 0){
            query.addCriteria(Criteria.where(historyFieldName).in(value));
        }
        return query;
    }
}
