package org.clevertec.project.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class FilterForHistory {
    private String[] userNames;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private Date timestampFrom;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private Date timestampTo;
    private String[] operatingType;
    private String[] entityType;
    private Boolean[] isWaslStatus;
    private Boolean[] isWialonStatus;


    public FilterForHistory(String[] userNames, Date timestampFrom, Date timestampTo, String[] operatingType, String[] entityType, Boolean[] isWaslStatus, Boolean[] isWialonStatus) {
        this.userNames = userNames;
        this.timestampFrom = timestampFrom;
        this.timestampTo = timestampTo;
        this.operatingType = operatingType;
        this.entityType = entityType;
        this.isWaslStatus = isWaslStatus;
        this.isWialonStatus = isWialonStatus;
    }

    public FilterForHistory() {
    }
}
