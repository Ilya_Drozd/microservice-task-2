package org.clevertec.project.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;

import java.util.Date;

@Data
@Accessors(chain = true)
public class History {
    @Id
    private String id;
    private String userName;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private Date timestamp;
    private String operatingType;
    private String entityType;
    private Boolean isWaslStatus;
    private Boolean isWialonStatus;
    private String waslDescription;
    private String wialonDescription;
    private String entityDescription;

    public History(String id, String userName, Date timestamp, String operatingType, String entityType,
                   Boolean isWaslStatus, Boolean isWialonStatus, String waslDescription, String wialonDescription,
                   String entityDescription) {
        this.id = id;
        this.userName = userName;
        this.timestamp = timestamp;
        this.operatingType = operatingType;
        this.entityType = entityType;
        this.isWaslStatus = isWaslStatus;
        this.isWialonStatus = isWialonStatus;
        this.waslDescription = waslDescription;
        this.wialonDescription = wialonDescription;
        this.entityDescription = entityDescription;
    }

    public History() {
    }
}
