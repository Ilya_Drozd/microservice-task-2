package org.clevertec.project.controller;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.clevertec.project.dto.FilterForHistory;
import org.clevertec.project.dto.History;
import org.clevertec.project.service.HistoryService;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(HistoryController.class)
public class HistoryControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private HistoryService historyService;

    private final History history = new History("1", "Ilya", new Date(), "registration", "company",
            true, true, "description", "description", "description");
    private final List<History> list = Arrays.asList(
            new History("1", "Ilya", new Date(), "registration", "company", true,
                    true, "description", "description", "description"),
            new History("2", "Petr", new Date(), "registration", "company", false,
                    true, "description", "description", "description"),
            new History("3", "Joseph", new Date(), "registration", "company", true,
                    false, "description", "description", "description"));
    private Page<History> page = new PageImpl<>(list);

    private String[] userNames = {"Ilya", "Petr", "Joseph"};
    private Date timestampFrom = new Date();
    private Date timestampTo = new Date();
    private String[] operatingType = {"registration", "deletion"};
    private String[] entityType = {"company", "vehicle", "driver"};
    private Boolean[] isWaslStatus = {true, false};
    private Boolean[] isWialonStatus = {true, false};
    private final FilterForHistory filter = new FilterForHistory(userNames, timestampFrom, timestampTo, operatingType,
            entityType, isWaslStatus, isWialonStatus);

    @Test
    public void postRequestSaveHistoryAndExpectStatusIsOk() throws Exception {
        given(this.historyService.save(history))
                .willReturn(history.getId());

        this.mockMvc.perform(post("http://localhost:9000/history/add")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(objectMapper.writeValueAsString(history)))
                .andExpect(status().isOk());
    }

    @Test
    public void postRequestSaveHistoryWithExceptionAndExpectInternalServerError() throws Exception {
        given(this.historyService.save(history))
                .willThrow(new RuntimeException());

        this.mockMvc.perform(post("http://localhost:9000/history/add")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(objectMapper.writeValueAsString(history)))
                .andExpect(status().isInternalServerError());
    }

    @Test
    public void getRequestFindHistoryAndExpectStatusIsOkAndContentJSON() throws Exception {
        given(this.historyService.findById("1")).willReturn(java.util.Optional.of(history));

        this.mockMvc.perform(get("http://localhost:9000/history/get/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

    @Test
    public void getRequestFindHistoryAndExpectJsonPathIdUserNameOperatingTypeEntityType() throws Exception {
        given(this.historyService.findById("1")).willReturn(java.util.Optional.of(history));

        this.mockMvc.perform(get("http://localhost:9000/history/get/1"))
                .andExpect(MockMvcResultMatchers.jsonPath("id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("id", CoreMatchers.equalTo("1")))
                .andExpect(MockMvcResultMatchers.jsonPath("userName").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("userName", CoreMatchers.equalTo("Ilya")))
                .andExpect(MockMvcResultMatchers.jsonPath("operatingType").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("operatingType", CoreMatchers.equalTo("registration")))
                .andExpect(MockMvcResultMatchers.jsonPath("entityType").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("entityType", CoreMatchers.equalTo("company")));
    }

    @Test
    public void getRequestFindNotExistHistoryAndExpectStatusIsNotFound() throws Exception {
        given(this.historyService.findById("2")).willReturn(Optional.empty());

        this.mockMvc.perform(get("http://localhost:9000/history/get/2"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void getRequestFindAllHistoriesPageableAndExpectStatusIsOk() throws Exception {
        given(this.historyService.findAll(anyInt(), anyInt())).willReturn(page);

        this.mockMvc.perform(get("http://localhost:9000/history/get/all?pagesize=10&pagenumber=0")
                .param("pagenumber", "0")
                .param("pagesize", "10"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

    @Test
    public void postRequestFindSomeHistoriesPageableAndExpectStatusIsOk() throws Exception {
        given(this.historyService.findSome(5, 0, filter)).willReturn(page);

        this.mockMvc.perform(post("http://localhost:9000/history/get/some?pagesize=5&pagenumber=0")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(objectMapper.writeValueAsString(filter))
                .param("pagenumber", "0")
                .param("pagesize", "5"))
                .andExpect(status().isOk());
    }

    @Test
    public void putRequestUpdateHistoryAndExpectStatusIsOk() throws Exception {
        given(this.historyService.findById("1")).willReturn(java.util.Optional.of(history));
        given(this.historyService.update(history)).willReturn(history.getId());

        this.mockMvc.perform(put("http://localhost:9000/history/update")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(objectMapper.writeValueAsString(history)))
                .andExpect(status().isOk());
    }

    @Test
    public void putRequestUpdateNotExistHistoryAndExpectStatusNotFound() throws Exception {
        given(this.historyService.findById("2")).willReturn(Optional.empty());

        this.mockMvc.perform(put("http://localhost:9000/history/update")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(objectMapper.writeValueAsString(history)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void deleteRequestDeleteHistoryAndExpectStatusIsOk() throws Exception {
        given(this.historyService.findById("1")).willReturn(java.util.Optional.of(history));
        given(this.historyService.delete("1")).willReturn("1");

        this.mockMvc.perform(delete("http://localhost:9000/history/delete/1"))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteRequestDeleteNotExistHistoryAndExpectStatusIsNotFound() throws Exception {
        given(this.historyService.findById("2")).willReturn(Optional.empty());

        this.mockMvc.perform(delete("http://localhost:9000/history/delete/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void deleteRequestDeleteAllHistoriesAndExpectStatusIsOk() throws Exception {
        given(this.historyService.deleteAll()).willReturn(6);

        this.mockMvc.perform(delete("http://localhost:9000/history/delete/all"))
                .andExpect(status().isOk());
    }
}