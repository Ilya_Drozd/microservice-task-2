# History service

## Description
Microservice for working with histories

## REST-services:
        
#### POST [http://localhost:9000/history/add](http://localhost:9000/history/add)
#### registration of a history

        Example of request body:
        {
        	"userName": "Maks",
        	"timestamp": "2017-04-30T10:15:39.476Z",
        	"operatingType": "registration",
        	"entityType": "driver",
        	"isWaslStatus": true,
        	"isWialonStatus": false,
        	"waslDescription": "description",
        	"wialonDescription": "description",
        	"entityDescription": "description"
        }

---

#### GET [http://localhost:9000/history/get/all?pagesize={pagesize}&pagenumber={pagenumber}](http://localhost:9000/history/get/all?pagesize={pagesize}&pagenumber={pagenumber})
#### receiving all histories

        where:
        {pagesize} - number of elements per page (optional parameter, default 15)
        {pagenumber} - page number (optional parameter, default 1)

---

#### GET [http://localhost:9000/history/get/{historyId}](http://localhost:9000/history/get/{historyId})
#### receiving history by id

        where:
        {historyId} - identity number

---

#### POST [http://localhost:9000/history/get/some?pagesize={pagesize}&pagenumber={pagenumber}](http://localhost:9000/history/get/some?pagesize={pagesize}&pagenumber={pagenumber})
#### receiving histories by different parameters

        where:
                {pagesize} - number of elements per page (optional parameter, default 15)
                {pagenumber} - page number (optional parameter, default 0)
                
        Example of request body:
        {
            "userNames": ["Maks, Joseph"],
            "timestampFrom": "2017-04-01T10:15:39.476Z",
            "timestampTo": "2020-04-30T16:12:39.476Z",
            "operatingType": ["registration", "deletion"],
            "entityType": ["company", "vehicle", "driver"],
            "isWaslStatus": ["true", "false"],
            "isWialonStatus": ["true", "false"]
        }

---

#### PUT [http://localhost:9000/history/update](http://localhost:9000/history/update)
#### history update

        Example of request body:
        {
        	"id": "5d1b781980e2011dd4714b61",
        	"userName": "Maks",
        	"timestamp": "2017-04-30T10:15:39.476Z",
        	"operatingType": "registration",
        	"entityType": "driver",
        	"isWaslStatus": false,
        	"isWialonStatus": true,
        	"waslDescription": "description",
        	"wialonDescription": "description",
        	"entityDescription": "description"
        }

---

#### DELETE [http://localhost:9000/history/delete/{historyId}](http://localhost:9000/history/delete/{historyId})
#### history delete

        where:
        {historyId} - identity number

---

#### DELETE [http://localhost:9000/history/delete/all](http://localhost:9000/history/delete/all)
#### all history deleting

---

  